import java.util.ArrayList;

public interface WordClass {
	
	public void deleteInflections();
	
	public void setWords(ArrayList<String> al);
	
	public ArrayList<String> getNormalizedWords();
}
