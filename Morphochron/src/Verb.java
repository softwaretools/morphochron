import java.util.ArrayList;

public class Verb implements WordClass{

 	private ArrayList<String> allVerbsOfCorpus = new ArrayList<String>();
 	private ArrayList<String> allWordsOfCorpus = new ArrayList<String>();
 	
	public void deleteInflections()
 	{
		for (String word : allWordsOfCorpus)
		{
			//inflections are left since word are found in OED and monomorphemic except 3.P Sing, which is not marked in PENN
	 		//[a-zA-Z]+[_|/](MD|MD0|VAG|VAN|VB|VBI|VBD|VBN|VBP)
			if (word.matches("[a-zA-Z]+[_|/](VBN)"))
	 		{
	 			word = word.replaceAll("[_|/](MD0|VAG|VAN|VBI|VBD|VBN|VBP)","");
	 			word = word.replaceAll("[_|/](MD|VB)","");
				allVerbsOfCorpus.add(word.toLowerCase());
	 		}
		}
 	}
	
	public void setWords(ArrayList<String> al)
	{
		this.allWordsOfCorpus = al;
		deleteInflections();
 	}
	
	public ArrayList<String> getNormalizedWords()
	{
 		return allVerbsOfCorpus;
 	}

}
