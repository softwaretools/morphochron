import java.util.ArrayList;

public class WordClassFactory {
	
	public WordClass normalizeWords(String type, ArrayList<String> allWordsOfCorpus)
	{
		WordClass wc = null;
		if (type.equals("_nn01"))
		{
			wc = new Noun();
		}
		else if (type.equals("_vb01"))
		{
			wc = new Verb();
		}
		else if (type.equals("_jj01"))
		{
			wc = new Adjective();
		}
		else
		{
			System.out.println("Undefined word class! Use _nn01, _vb01, or _jj01");
		}
		wc.setWords(allWordsOfCorpus);
		
		return wc;
	}
}
