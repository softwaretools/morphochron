import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class IO {
	// read the files of entire directory
	public ArrayList<String> readFilesFromDirectory(String path, String filter)
	{
		ArrayList<String> allWordsOfTexts = new ArrayList<String>();
		try (Stream<Path> paths = Files.walk(Paths.get(path))) {
			paths.filter(Files::isRegularFile).
			filter(p -> p.getFileName().toString().matches(filter)).
			forEach(item ->
			{
				//System.out.println(item.getFileName().toString() );	
				try (Scanner s = new Scanner(new File(item.toString())).useDelimiter("\\s+")) 
					{
					    while (s.hasNext()) 
					    {
					    	allWordsOfTexts.add(s.next());
					    }
					}
					catch (FileNotFoundException e) {}
			});
		
		}
		catch (IOException e) {}
		
		return allWordsOfTexts;
	}
	
	
	public String readFile(String file, boolean unicode)
	  {

		String data = "";
	    String line = "";
	    char cData[] = new char[5*1048576];
	    int i = 0;
	    try
	    {
	      BufferedReader fr =
	            new BufferedReader(
	                new InputStreamReader(new FileInputStream(file),
	                                unicode?"UnicodeLittle":"ISO8859_1"));
	      i = fr.read(cData);
	      data = new String(cData, 0, i);
	      fr.close();
	    } catch(IOException e){}
	    return(data);
	  }

	  static void writeFile(String file, String data, boolean unicode)
	  {
	    try
	    {
	      BufferedWriter bw = new BufferedWriter(unicode?new OutputStreamWriter(new FileOutputStream(file),"UnicodeLittle"):new FileWriter(file));
	      bw.write(data);
	      bw.close();
	    } catch(IOException e){}
	  }
	  
	  static Map<String, ArrayList<String>> readMorphemeWordListFromCSVFile(String filepath) throws IOException
	  {
		  
		  Map<String, ArrayList<String>> morphemeWordList = new HashMap<String, ArrayList<String>>();
		    try(BufferedReader br = new BufferedReader(new FileReader(filepath))) {
		        String line = "";
		        while ((line = br.readLine()) != null) {
		        	ArrayList<String> content = new ArrayList<String>();
		        	String [] data = line.split(";");
		        	for (int i=1;i<data.length;i++) 
		        	{
		        		content.add(data[i]);
		        	}
		            morphemeWordList.put(data[0],content);
		        }
		    } catch (FileNotFoundException e) {
		      //Some error logging
		    }
		    
		  return morphemeWordList;
	  }
	  
	  public void writeMorphemeWordListToCSVFile(String filepath, Map<String, ArrayList<String>> morphemeWordList)
	  {
		  try
		  {
	            FileWriter file = new FileWriter(filepath);
	            PrintWriter write = new PrintWriter(file);
	            
	            for (String s : morphemeWordList.keySet())
	    		{
	    			String key = s.toString();
	    		    ArrayList<String> value = morphemeWordList.get(s);
	    		   
	    		    write.print(key + ";");
	    		    for (String v : value)
	    		    {
	    		    	write.print(v + ";");
	    		    }
	    		    write.println();
	    		}
	            write.close();
		   } 
		  catch (IOException e) 
		  {
			  System.out.println(e.getMessage());
		  }
	  }
	  
	  public void appendResultsToCSVFile(String filepath, String data) throws IOException 
	  {
		  try
		  {
	            FileWriter file = new FileWriter(filepath, true); //for overwriting set boolean to false
	            PrintWriter write = new PrintWriter(file);
	            write.println(data);
	            write.close();
		   } 
		  catch (IOException e) 
		  {
			  System.out.println(e.getMessage());
		  }
	  }
	  /*
	   * given a REST API URL and credentials, data are read from which the given URL points to
	   */
	  public String requestRESTfulAPI(String restUrl, String app_id, String app_key)
		{
			String jsonString = "";
			
			try 
			{         
				URL url = new URL(restUrl);         
				HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();         
				urlConnection.setRequestProperty("Accept", "application/json");         
				urlConnection.setRequestProperty("app_id", app_id);         
				urlConnection.setRequestProperty("app_key", app_key);
				
				// read the output from the server         
				BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));         
				StringBuilder stringBuilder = new StringBuilder();         
				String line = null;         
				while ((line = reader.readLine()) != null) 
				{             
					stringBuilder.append(line + "\n");        
				}
				jsonString = stringBuilder.toString();
				
				//System.out.println("retrieved OED entry: " + stringBuilder.toString());
			} 
			catch (IOException e) 
			{         
				e.printStackTrace();      
			} 
			return jsonString;
		}
}
