import java.util.ArrayList;

public class Noun implements WordClass
{
	private ArrayList<String> allNounsOfCorpus = new ArrayList<String>();
	private ArrayList<String> allPluralnounsOfCorpus = new ArrayList<String>();
	private ArrayList<String> allWordsOfCorpus = new ArrayList<String>();
	
	public void deleteInflections()
	{
		for (String word : allWordsOfCorpus)
		{
		if (word.endsWith("/N") 
				//Proper Nouns of all kinds are excluded
				// || word.endsWith("NPR") || word.endsWith("NPR$")
				// || word.endsWith("NPRS") || word.endsWith("NPRS$")
				// all forms of nominalized other, e.g. the other are excluded
				// || word.endsWith("OTHER") || word.endsWith("OTHER$") 
				// || word.endsWith("OTHERS") || word.endsWith("OTHERS$")
				)
		{	
			word = word.replace("/N", "");
			allNounsOfCorpus.add(word.toLowerCase());	
		}
		/*get rid of Possessives and Plural 
		 * (Plural nouns cannot be sorted out, 
		 * possible with a second loop but not 
		 * worthwhile since not containing lexical morphemes)*/
		
		else if (word.endsWith("/NS$"))
		{
			word = word.replace("ies/NS$", "y");
			word = word.replace("ies'/NS$", "y");
			word = word.replace("ches/NS$", "ch");
			word = word.replace("ches'/NS$", "ch");
			word = word.replace("ses/NS$", "s");
			word = word.replace("ses'/NS$", "s");
			word = word.replace("shes/NS$", "sh");
			word = word.replace("shes'/NS$", "sh");
			word = word.replace("./NS$", "");
			word = word.replace("s'/NS$", "");
			word = word.replace("'/NS$", "");
			word = word.replace("'s/NS$", "");
			word = word.replace("s/NS$", "");
			allNounsOfCorpus.add(word.toLowerCase());
		}
		//get rid of Possessives
		else if (word.endsWith("/N$"))
		{
			word = word.replace("'s./N$", "");
			word = word.replace("./N$", "");
			word = word.replace("'s/N$", "");
			word = word.replace("s/N$", "");
			word = word.replace("'/N$", "");
			word = word.replace("/N$", "");
			allNounsOfCorpus.add(word.toLowerCase());
		}
		//get rid of Plural
		else if (word.endsWith("/NS"))		
		{
			//System.out.println(word);
			word = word.replace("ies/NS", "y");
			word = word.replace("ches/NS", "ch");
			word = word.replace("ses/NS", "s");
			word = word.replace("shes/NS", "sh");
			word = word.replace("./NS", "");
			word = word.replace("s/NS", "");
			word = word.replace("s'/NS", "");
			word = word.replace("'/NS", "");
			if (word.endsWith("/NS"))
			{
				word = word.replace("/NS", "");
				allPluralnounsOfCorpus.add(word);
			}
			allNounsOfCorpus.add(word.toLowerCase());
		}
		}
	}
	
	public void setWords(ArrayList<String> al)
	{
		this.allWordsOfCorpus = al;
		deleteInflections();
	}
	
	public ArrayList<String> getNormalizedWords()
	{
		return allNounsOfCorpus;
	}
}
