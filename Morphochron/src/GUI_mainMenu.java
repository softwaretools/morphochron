import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class GUI_mainMenu extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCorpusDir;
	private JTextField textFieldResultDir;
	private javax.swing.JFileChooser selectCorpus;
	private javax.swing.JFileChooser selectResultDir;
	private JTextArea resultTextArea;
	private JTextField textFieldID;
	private JTextField textFieldKey;
	private javax.swing.JFileChooser selectCredentialsID;
	private javax.swing.JFileChooser selectCredentialsKey;
	private JCheckBox chckbxM1;
	private JCheckBox chckbxM2;
	private JCheckBox chckbxM3;
	private JCheckBox chckbxM4;
	private JCheckBox chckbxE1;
	private JCheckBox chckbxE2;
	private JCheckBox chckbxE3;
	private JCheckBox chckbxEmod1;
	private JCheckBox chckbxEmod2;
	private JCheckBox chckbxEmod3;
	private JRadioButton rdbtnPrefix;
	private JRadioButton rdbtnSuffix;
	private JRadioButton rdbtnAdjective;
	private JRadioButton rdbtnNoun;
	private JRadioButton rdbtnVerb;
	private String corpus = "";
	private String wordclass ="";
	private String affixtype = "";
	private String corpusPath = "";
	private String resultPath = "";
	private String app_id = "";
	private String app_key = "";
	private Set<String> period = new HashSet<String>();
	private JButton btnRun = new JButton("Run");
	private ButtonGroup affixGroup;
	private ButtonGroup wordclassGroup;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					GUI_mainMenu frame = new GUI_mainMenu();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	public void setMessage(String msg)
	{
		resultTextArea.append(msg);
	}
	
	public String getAppID()
	{
		return app_id;
	}
	
	public String getAppKey()
	{
		return app_key;
	}
	
	public String getWordClass()
	{
		return wordclass;
	}
	
	public String getAffixType()
	{
		return affixtype;
	}
	
	public Set<String> getPeriods()
	{
		return period;
	}
	
	public String getCorpus()
	{
		return corpus;
	}
	
	public String getResultPath()
	{
		return textFieldResultDir.getText();
	}
	
	public String getCorpusPath()
	{
		return textFieldCorpusDir.getText();
	}
	
	public JButton getRunButton()
	{
		return btnRun;
	}
	
	private String getOSPathCorpora()
	{
		if (System.getProperty("os.name").startsWith("Windows"))
		{
			return "C:\\Users\\Peukert\\Corpora\\PENN Corpus\\PENN-CORPORA";
		}
		else
		{
			return "/home/amadeus/Dokumente/Corpora/PENN Corpus/PENN-CORPORA";
		}	
	}
	
	private String getOSPathID()
	{
		if (System.getProperty("os.name").startsWith("Windows"))
		{
			return "C:\\Users\\Peukert\\Documents\\Morphochron";
		}
		else
		{
			return "/home/amadeus/Dokumente/Projekte/Morphochron";
		}	
	}
	/**
	 * Create the frame.
	 */
	public GUI_mainMenu() {
		setTitle("Morphochron");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 650, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		
		textFieldCorpusDir = new JTextField();
		textFieldCorpusDir.setBounds(22, 46, 498, 20);
		textFieldCorpusDir.setColumns(10);
		
		textFieldResultDir = new JTextField();
		textFieldResultDir.setBounds(22, 96, 498, 20);
		textFieldResultDir.setColumns(10);
		
		selectCorpus = new javax.swing.JFileChooser();
		selectResultDir = new javax.swing.JFileChooser();
		selectCredentialsID = new javax.swing.JFileChooser();
		selectCredentialsKey = new javax.swing.JFileChooser();
		
		JLabel lblCorpusDirectory = new JLabel("Select Corpus Directory");
		lblCorpusDirectory.setBounds(22, 27, 116, 14);
		
		JButton btnSelectResultDirectory = new JButton("Select Result");
		btnSelectResultDirectory.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				selectResultDir.setCurrentDirectory(new File(getOSPathID()));
				//selectResultDir.setFileFilter( new FileNameExtensionFilter(".","pos")) ;
				selectResultDir.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		        int action = selectResultDir.showOpenDialog(btnSelectResultDirectory);
		        if (action == JFileChooser.APPROVE_OPTION)
		        {
		            File location = selectResultDir.getSelectedFile();
//		            dictName = location.getAbsolutePath().toString();
		            resultPath = location.getAbsolutePath();//getParent();		            
		        }
		        
		        //selection is displayed at the left
		        textFieldResultDir.setText(resultPath);
			}
		});
		btnSelectResultDirectory.setBounds(527, 95, 99, 23);
		contentPane.setLayout(null);
		
		JButton btnSelectCorpusDirectory = new JButton("Select Corpus");
		btnSelectCorpusDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				selectCorpus.setCurrentDirectory(new File(getOSPathCorpora()));
				//selectCorpus.setFileFilter( new FileNameExtensionFilter(".","pos")) ;
		        selectCorpus.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		        int action = selectCorpus.showOpenDialog(btnSelectCorpusDirectory);
		        if (action == JFileChooser.APPROVE_OPTION)
		        {
		            File location = selectCorpus.getSelectedFile();
//		            dictName = location.getAbsolutePath().toString();
		            corpusPath = location.getParent();		            
		        }
		        
		        //selection is displayed at the left
		        textFieldCorpusDir.setText(corpusPath);
		        if (corpusPath.matches(".*PPCMBE-RELEASE-1.*"))
		        {
		        	corpus = "ppcmbe";
		        	chckbxM1.setEnabled(false);
		        	chckbxM2.setEnabled(false);
		        	chckbxM3.setEnabled(false);
		        	chckbxM4.setEnabled(false);
		        	chckbxM1.setSelected(false);
		        	chckbxM2.setSelected(false);
		        	chckbxM3.setSelected(false);
		        	chckbxM4.setSelected(false);
		        	
		        	chckbxE1.setEnabled(false);
		        	chckbxE2.setEnabled(false);
		        	chckbxE3.setEnabled(false);
		        	chckbxE1.setSelected(false);
		        	chckbxE2.setSelected(false);
		        	chckbxE3.setSelected(false);
		        	
		        	chckbxEmod1.setSelected(false);
		        	chckbxEmod2.setSelected(false);
		        	chckbxEmod3.setSelected(false);
		        	chckbxEmod1.setEnabled(true);
		        	chckbxEmod2.setEnabled(true);
		        	chckbxEmod3.setEnabled(true);
		        	period.clear();
		        }
		        else if (corpusPath.matches(".*PPCEME-RELEASE-2.*"))
		        {
		        	corpus = "ppceme";
		        	chckbxM1.setEnabled(false);
		        	chckbxM2.setEnabled(false);
		        	chckbxM3.setEnabled(false);
		        	chckbxM4.setEnabled(false);
		        	chckbxM1.setSelected(false);
		        	chckbxM2.setSelected(false);
		        	chckbxM3.setSelected(false);
		        	chckbxM4.setSelected(false);
		        	
		        	chckbxE1.setEnabled(true);
		        	chckbxE2.setEnabled(true);
		        	chckbxE3.setEnabled(true);
		        	chckbxE1.setSelected(false);
		        	chckbxE2.setSelected(false);
		        	chckbxE3.setSelected(false);
		        	
		        	chckbxEmod1.setSelected(false);
		        	chckbxEmod2.setSelected(false);
		        	chckbxEmod3.setSelected(false);
		        	chckbxEmod1.setEnabled(false);
		        	chckbxEmod2.setEnabled(false);
		        	chckbxEmod3.setEnabled(false);
		        	period.clear();
		        }
		        else if (corpusPath.matches(".*PPCME2-RELEASE-3.*"))
		        {
		        	corpus = "ppcmb2";
		        	chckbxM1.setEnabled(true);
		        	chckbxM2.setEnabled(true);
		        	chckbxM3.setEnabled(true);
		        	chckbxM4.setEnabled(true);
		        	chckbxM1.setSelected(false);
		        	chckbxM2.setSelected(false);
		        	chckbxM3.setSelected(false);
		        	chckbxM4.setSelected(false);
		        	
		        	chckbxE1.setEnabled(false);
		        	chckbxE2.setEnabled(false);
		        	chckbxE3.setEnabled(false);
		        	chckbxE1.setSelected(false);
		        	chckbxE2.setSelected(false);
		        	chckbxE3.setSelected(false);
		        	
		        	chckbxEmod1.setSelected(false);
		        	chckbxEmod2.setSelected(false);
		        	chckbxEmod3.setSelected(false);
		        	chckbxEmod1.setEnabled(false);
		        	chckbxEmod2.setEnabled(false);
		        	chckbxEmod3.setEnabled(false);
		        	period.clear();
		        }
		        else
		        {
		        	chckbxM1.setEnabled(false);
		        	chckbxM2.setEnabled(false);
		        	chckbxM3.setEnabled(false);
		        	chckbxM4.setEnabled(false);
		        	chckbxM1.setSelected(false);
		        	chckbxM2.setSelected(false);
		        	chckbxM3.setSelected(false);
		        	chckbxM4.setSelected(false);
		        	
		        	chckbxE1.setEnabled(false);
		        	chckbxE2.setEnabled(false);
		        	chckbxE3.setEnabled(false);
		        	chckbxE1.setSelected(false);
		        	chckbxE2.setSelected(false);
		        	chckbxE3.setSelected(false);
		        	
		        	chckbxEmod1.setSelected(false);
		        	chckbxEmod2.setSelected(false);
		        	chckbxEmod3.setSelected(false);
		        	chckbxEmod1.setEnabled(false);
		        	chckbxEmod2.setEnabled(false);
		        	chckbxEmod3.setEnabled(false);
		        	period.clear();
		        }
			}
		});
		btnSelectCorpusDirectory.setBounds(527, 45, 99, 23);
		contentPane.add(btnSelectCorpusDirectory, "9, 1, left, top");
		contentPane.add(textFieldCorpusDir, "5, 2, left, center");
		contentPane.add(textFieldResultDir, "9, 2, left, center");
		contentPane.add(lblCorpusDirectory, "11, 2, left, center");
		
		JLabel lblSelectResultDirectory = new JLabel("Select Result Directory");
		lblSelectResultDirectory.setBounds(25, 77, 113, 14);
		contentPane.add(lblSelectResultDirectory, "12, 2, left, center");
		contentPane.add(btnSelectResultDirectory, "13, 2, left, top");
		
		JLabel lblCredentialID = new JLabel("Enter OED ID or load from file");
		lblCredentialID.setBounds(28, 130, 269, 14);
		contentPane.add(lblCredentialID);
		
		textFieldID = new JTextField();
		textFieldID.setBounds(22, 152, 498, 20);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);
		
		JButton btnCredentialsID = new JButton("Load ID");
		btnCredentialsID.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				selectCredentialsID.setCurrentDirectory(new File(getOSPathID()));
				//selectCredentialsID.setFileFilter( new FileNameExtensionFilter(".","txt")) ;
				selectCredentialsID.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		        int action = selectCredentialsID.showOpenDialog(btnCredentialsID);
		        if (action == JFileChooser.APPROVE_OPTION)
		        {
		            File location = selectCredentialsID.getSelectedFile();
		            String idName = location.getAbsolutePath().toString();
		            IO credentials = new IO();
		            app_id = credentials.readFile(idName, false);
		        }		        
		        //selection is displayed at the left
		        textFieldID.setText(app_id);
			}
		});
		btnCredentialsID.setBounds(527, 151, 99, 23);
		contentPane.add(btnCredentialsID);
		
		textFieldKey = new JTextField();
		textFieldKey.setColumns(10);
		textFieldKey.setBounds(22, 205, 498, 20);
		contentPane.add(textFieldKey);
		
		JLabel lblCredentialKey = new JLabel("Enter OED key or load from file");
		lblCredentialKey.setBounds(22, 183, 269, 14);
		contentPane.add(lblCredentialKey);
		
		JButton btnCredentialsKey = new JButton("Load Key");
		btnCredentialsKey.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				selectCredentialsKey.setCurrentDirectory(new File(getOSPathID()));
				//selectCredentialsKey.setFileFilter( new FileNameExtensionFilter(".","txt")) ;
				selectCredentialsKey.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		        int action = selectCredentialsKey.showOpenDialog(btnCredentialsKey);
		        if (action == JFileChooser.APPROVE_OPTION)
		        {
		            File location = selectCredentialsKey.getSelectedFile();
		            String idName = location.getAbsolutePath().toString();
		            IO credentials = new IO();
		            app_key = credentials.readFile(idName, false);
		        }		        
		        //selection is displayed at the left
		        textFieldKey.setText(app_key);
			}
		});
		btnCredentialsKey.setBounds(527, 204, 99, 23);
		contentPane.add(btnCredentialsKey);
		
		chckbxM1 = new JCheckBox("M1");
		chckbxM1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxM1.isSelected()) 
				{
					period.add("m1");
				}
				else
				{
					period.remove("m1");
				}
			}
		});
		chckbxM1.setBounds(22, 264, 49, 23);
		chckbxM1.setEnabled(false);
		contentPane.add(chckbxM1);
		
		chckbxM2 = new JCheckBox("M2");
		chckbxM2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxM1.isSelected()) 
				{
					period.add("m2");
				}
				else
				{
					period.remove("m2");
				}
			}
		});
		chckbxM2.setEnabled(false);
		chckbxM2.setBounds(22, 289, 54, 23);
		contentPane.add(chckbxM2);
		
		chckbxM3 = new JCheckBox("M3");
		chckbxM3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxM3.isSelected()) 
				{
					period.add("m3");
				}
				else
				{
					period.remove("m3");
				}
			}
		});
		chckbxM3.setEnabled(false);
		chckbxM3.setBounds(22, 317, 49, 23);
		contentPane.add(chckbxM3);
		
		chckbxM4 = new JCheckBox("M4");
		chckbxM4.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxM4.isSelected()) 
				{
					period.add("m4");
				}
				else
				{
					period.remove("m4");
				}
			}
		});
		chckbxM4.setEnabled(false);
		chckbxM4.setBounds(22, 343, 49, 23);
		contentPane.add(chckbxM4);
		
		chckbxE1 = new JCheckBox("E1");
		chckbxE1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxE1.isSelected()) 
				{
					period.add("e1");
				}
				else
				{
					period.remove("e1");
				}
			}
		});
		chckbxE1.setBounds(99, 264, 49, 23);
		chckbxE1.setEnabled(false);
		contentPane.add(chckbxE1);
		
		chckbxE2 = new JCheckBox("E2");
		chckbxE2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxE2.isSelected()) 
				{
					period.add("e2");
				}
				else
				{
					period.remove("e2");
				}
			}
		});
		chckbxE2.setBounds(99, 289, 41, 23);
		chckbxE2.setEnabled(false);
		contentPane.add(chckbxE2);
		
		chckbxE3 = new JCheckBox("E3");
		chckbxE3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxE3.isSelected()) 
				{
					period.add("e3");
				}
				else
				{
					period.remove("e3");
				}
			}
		});
		chckbxE3.setBounds(99, 317, 41, 23);
		chckbxE3.setEnabled(false);
		contentPane.add(chckbxE3);
		
		chckbxEmod1 = new JCheckBox("Emod1");
		chckbxEmod1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxEmod1.isSelected()) 
				{
					period.add("Emod1");
				}
				else
				{
					period.remove("Emod1");
				}
			}
		});
		chckbxEmod1.setBounds(171, 264, 59, 23);
		chckbxEmod1.setEnabled(false);
		contentPane.add(chckbxEmod1);
		
		chckbxEmod2 = new JCheckBox("Emod2");
		chckbxEmod2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxEmod2.isSelected()) 
				{
					period.add("Emod2");
				}
				else
				{
					period.remove("Emod2");
				}
			}
		});
		chckbxEmod2.setBounds(171, 289, 59, 23);
		chckbxEmod2.setEnabled(false);
		contentPane.add(chckbxEmod2);
		
		chckbxEmod3 = new JCheckBox("Emod3");
		chckbxEmod3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (chckbxEmod3.isSelected()) 
				{
					period.add("Emod3");
				}
				else
				{
					period.remove("Emod3");
				}
			}
		});
		chckbxEmod3.setBounds(171, 317, 59, 23);
		chckbxEmod3.setEnabled(false);
		contentPane.add(chckbxEmod3);
		
		JRadioButton rdbtnPrefix = new JRadioButton("Prefix");
		rdbtnPrefix.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				affixtype = "_pr01";
			}
		});
		rdbtnPrefix.setBounds(416, 264, 111, 23);
		contentPane.add(rdbtnPrefix);
		
		JRadioButton rdbtnSuffix = new JRadioButton("Suffix");
		rdbtnSuffix.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				affixtype = "_su01";
			}
		});
		rdbtnSuffix.setBounds(416, 289, 111, 23);
		contentPane.add(rdbtnSuffix);
		
		affixGroup = new ButtonGroup();
		affixGroup.add(rdbtnPrefix);
		affixGroup.add(rdbtnSuffix);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		btnCancel.setBounds(438, 343, 89, 23);
		contentPane.add(btnCancel);

		btnRun.setBounds(537, 343, 89, 23);
		contentPane.add(btnRun);
		
		JLabel lblppcme2 = new JLabel("PPCME2");
		lblppcme2.setBounds(22, 243, 59, 14);
		contentPane.add(lblppcme2);
		
		JLabel lblppceme = new JLabel("PPCEME");
		lblppceme.setBounds(99, 243, 62, 14);
		contentPane.add(lblppceme);
		
		JLabel lblppcmbe = new JLabel("PPCMBE");
		lblppcmbe.setBounds(171, 243, 59, 14);
		contentPane.add(lblppcmbe);
		
		JLabel lblAffixType = new JLabel("Affix Type");
		lblAffixType.setBounds(418, 243, 49, 14);
		contentPane.add(lblAffixType);
		
		JLabel lblResults = new JLabel("Results");
		lblResults.setBounds(22, 371, 49, 14);
		contentPane.add(lblResults);
		//contentPane.add(scrollPane);
		
		JLabel lblWordclass = new JLabel("Word Class");
		lblWordclass.setBounds(284, 243, 99, 14);
		contentPane.add(lblWordclass);
		
		JRadioButton rdbtnNoun = new JRadioButton("Noun");
		rdbtnNoun.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				wordclass = "_nn01";
			}
		});
		rdbtnNoun.setBounds(291, 264, 111, 23);
		contentPane.add(rdbtnNoun);
		
		JRadioButton rdbtnVerb = new JRadioButton("Verb");
		rdbtnVerb.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				wordclass = "_vb01";
			}
		});
		rdbtnVerb.setBounds(291, 289, 111, 23);
		contentPane.add(rdbtnVerb);
		
		JRadioButton rdbtnAdjective = new JRadioButton("Adjective");
		rdbtnAdjective.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				wordclass = "_jj01";
			}
		});
		rdbtnAdjective.setBounds(291, 317, 111, 23);
		contentPane.add(rdbtnAdjective);
		
		wordclassGroup = new ButtonGroup();
		wordclassGroup.add(rdbtnNoun);
		wordclassGroup.add(rdbtnVerb);
		wordclassGroup.add(rdbtnAdjective);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 396, 601, 140);
		contentPane.add(scrollPane);
		
		resultTextArea = new JTextArea();
		resultTextArea.setEditable(false);
		scrollPane.setViewportView(resultTextArea);
		resultTextArea.setAutoscrolls(true);
		
	}
	
	public boolean validateForm() {

	    StringBuilder errors = new StringBuilder();
	    IO testapi = new IO();
	    // Confirm mandatory fields are filled out
	    if (textFieldCorpusDir.getText().trim().isEmpty()) {
	        errors.append("- Please enter a PPC directory.\n");
	        textFieldCorpusDir.requestFocusInWindow();
	        JOptionPane.showMessageDialog(null, errors, "Directory not specified!", JOptionPane.ERROR_MESSAGE);
	        return false;
	    }
	    else
	     if (textFieldResultDir.getText().trim().isEmpty()) {
	        errors.append("- Please enter a result directory.\n");
	        textFieldResultDir.requestFocusInWindow();
	        JOptionPane.showMessageDialog(null, errors, "Directory not specified!", JOptionPane.ERROR_MESSAGE);
	        return false;
	    }
	     else if (textFieldID.getText().trim().isEmpty())
	     {
	    	 errors.append("- Please enter a valid OED ID.\n");
	    	 textFieldID.requestFocusInWindow();
		     JOptionPane.showMessageDialog(null, errors, "No OED ID available!", JOptionPane.ERROR_MESSAGE);
		     return false;
	     }
	     else if (textFieldKey.getText().trim().isEmpty())
	     {
	    	 errors.append("- Please enter a valid OED ID.\n");
	    	 textFieldKey.requestFocusInWindow();
		     JOptionPane.showMessageDialog(null, errors, "No OED ID available!", JOptionPane.ERROR_MESSAGE);
		     return false;
	     }
	     else if ( affixGroup.getSelection() == null )
	     {
	    	 errors.append("- Please select the affix type.\n");
		     JOptionPane.showMessageDialog(null, errors, "No Affix type available!", JOptionPane.ERROR_MESSAGE);
		     return false;
	     }
	     else if (wordclassGroup.getSelection() == null)
	     {
	    	 errors.append("- Please select the word class.\n");
		     JOptionPane.showMessageDialog(null, errors, "No word class available!", JOptionPane.ERROR_MESSAGE);
		     return false;
	     }
	     else if (testapi.requestRESTfulAPI("https://oed-researcher-api.oxfordlanguages.com/oed/api/v0.2/words/?lemma=test", app_id, app_key).isEmpty())
	     {
	    	 errors.append("- Your OED credentials are invalid.\n");
		     JOptionPane.showMessageDialog(null, errors, "No valid OED access!", JOptionPane.ERROR_MESSAGE);
		     return false;
	     }
	    else
	     {
	    	 return true;
	     }
    }
}
