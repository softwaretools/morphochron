
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.json.JSONArray;

public class OED 

/**
 * to get the word_ID right, concatenate the word with the following codes
 * - nn01 for Noun
 * - vb01 for Verb
 * - jj01 for Adjective
 * - rb01 for Adverb
 * - su01 for Suffix
 * - pr01 for Prefix
 * the running number orders the entries in the dictionary, i.e. the number of meanings
 */
{
	private String word = "";
	private String morpheme = "";
	private String wordclass = "";
	private String affixtype = "";
	private int enddateCorpus = 0;
	private int startdateCorpus = 0;
	private final String app_id;
	private final String app_key;
	private IO restapi = new IO();
	private String resultPath = "";
	private Corpus corpus;
	
	//for each look up the word and one of its contained affixes is needed
	public OED(String word, String morpheme, String wordclass, String affixtype, Corpus corpus, String app_key, String app_id, String resultPath)
	{
		this.corpus = corpus;
		this.app_id = app_id;//restapi.readFile("C:\\Users\\Peukert\\Documents\\Morphochron\\id", false); 
		this.app_key = app_key;//restapi.readFile("C:\\Users\\Peukert\\Documents\\Morphochron\\key", false);
		this.word = word;
		this.morpheme = morpheme;
		this.wordclass = wordclass;
		this.affixtype = affixtype;
		this.startdateCorpus = corpus.getStartDate();
		this.enddateCorpus = corpus.getEndDate();
		this.resultPath = resultPath;
	}
	
	/*
	 * gets lemmatized word representation of OED REST API as JSON object
	 */
	private String getRESTAPILemmatizedWord(String word)
	{
		return "https://oed-researcher-api.oxfordlanguages.com/oed/api/v0.2/lemmatize/?form=" + word;// + "&part_of_speech=" + wordclass;
	}
	
	/*
	 * gets word representation of OED REST API as JSON object
	 */
	private String getRESTAPIWordRepresentation(String word)
	{
		return "https://oed-researcher-api.oxfordlanguages.com/oed/api/v0.2/words/?lemma=" + word;
	}
	
	/*
	 * gets the roots OED representation of a word
	 */
	private String getRESTAPIRootRepresentation(String wordID)
	{
		return "https://oed-researcher-api.oxfordlanguages.com/oed/api/v0.2/word/" + wordID + "/roots/";
	}
	
	/*
	 * extracts ID from the OED word JSON response
	 * checks if ID corresponds to given word class
	 */
	private String processJSonWordID(JSONObject obj)
	{
		String wordid = "";
		JSONArray arr = obj.getJSONArray("data"); 
		for (int i = 0; i < arr.length(); i++)
		{
		    wordid = arr.getJSONObject(i).getJSONObject("word").getString("id");
		    //System.out.println("Wort-ID:" + wordid);
		    if (wordid.endsWith(wordclass)) break; //words may be part of several word classes
		}

		return wordid;
	}
	
	/* @deprecated
	 * checks if a word has an extra entry for the given morpheme
	 * checks if field daterange.obsolete = false and daterange.end is null
	 */
	private Boolean processJSonRoot(JSONObject obj)
	{
		int startyearOED = 0; 
		int endyearOED = 0;
		Boolean occurredIn = false;
		Boolean obsolete = true; 
		
		String affix = "";
		if (affixtype.equals("_pr01"))
		{
			affix = morpheme +"-";
		}
		else if (affixtype.equals("_su01"))
		{
			affix = "-" + morpheme;
		}
		else
		{
			System.out.println("Affix type not defined");
		}
		
		JSONArray arr = obj.getJSONArray("data"); 
		for (int i = 0; i < arr.length(); i++)
		{
			 
			//System.out.println("The following morpheme is checked for existence: " + morpheme);
			occurredIn = arr.getJSONObject(i).getString("lemma").equals(affix);
			if (occurredIn)
			{
				//System.out.println("The following morpheme was actually found: " + morpheme);
				endyearOED = arr.getJSONObject(i).getJSONObject("daterange").optInt("end", 10000);//lots of enddates are null,i.e. not set
				startyearOED = arr.getJSONObject(i).getJSONObject("daterange").optInt("start", 0);
				obsolete = arr.getJSONObject(i).getJSONObject("daterange").getBoolean("obsolete");
				break;
			}
		}

		// && obsolete not included because it seems to be today's perspective of obselete
		return (occurredIn && startyearOED < startdateCorpus && enddateCorpus < endyearOED);
	}
	
	private Set<String> getAllAffixesFromJSonRoot(JSONObject obj)
	{
		int startyearOED = 0; 
		int endyearOED = 0;
		Set<String> affixTypes = new HashSet<String>();
		
		JSONArray arr = obj.getJSONArray("data"); 
		for (int i = 0; i < arr.length(); i++)
		{
			String s = arr.getJSONObject(i).getString("lemma");
			if ((s.startsWith("-") && affixtype.equals("_su01") ) || (s.endsWith("-") && affixtype.equals("_pr01")))
			{
				endyearOED = arr.getJSONObject(i).getJSONObject("daterange").optInt("end", 10000);//lots of enddates are null,i.e. not set
				startyearOED = arr.getJSONObject(i).getJSONObject("daterange").optInt("start", 0);
				//obsolete = arr.getJSONObject(i).getJSONObject("daterange").getBoolean("obsolete");
				if (startyearOED < startdateCorpus && enddateCorpus < endyearOED)
				{
					affixTypes.add(s.replace("-", ""));
				}
			}
		}
		
		return affixTypes;
	}
	
	/*
	 * processes OED API queries
	 */
	private JSONObject getJSonResponse(String restUrl)
	{
		return new JSONObject(restapi.requestRESTfulAPI(restUrl, app_id, app_key));
	}
	
	/*
	 * 1. build URL for Word-REST API --> getRESTAPIWordRepresentation(String) String
	 * 2. get word JSON Format from Word-REST API --> getJSonResponse(String) JSONObject
	 * 3a. get the wordID out of the JSon --> processJSonWordID(JSONOBject) String
	 * 3b. get time range ?
	 * ---> same logic again <---
	 * 4. build URL for Root-REST API --> getRESTAPIRootRepresentation(String) String
	 * 5. get root JSON format from Root-REST API --> getJSonResponse(String) JSONObject
	 * 6a. get start date out of JSON --> processJSonRoot(JSONObject)
	 * 
	 */
	public Boolean processOEDRequest()
	{
		//Map<String, Integer> oedData = new HashMap<String,Integer>();
		Boolean entryAvailable = false;
		//String wordJSON = getRESTAPIWordRepresentation(word.toLowerCase());
		String wordJSON = getRESTAPILemmatizedWord(word.toLowerCase());
		JSONObject jo = getJSonResponse(wordJSON);
		String id = processJSonWordID(jo);
		if (!id.isEmpty())
		{
			String s = getRESTAPIRootRepresentation(id);
			JSONObject o =	getJSonResponse(s);
			entryAvailable = processJSonRoot(o);
		}
//		else
//		{
//			entryAvailable = false;
//		}
		return entryAvailable;
	}
	
	public Set<String> getMorphemesOEDRequest()
	{
		//String wordJSON = getRESTAPIWordRepresentation(word.toLowerCase());
		String wordJSON = getRESTAPILemmatizedWord(word.toLowerCase());
		JSONObject jo = getJSonResponse(wordJSON);
		String id = processJSonWordID(jo);
		Set<String> affixes = new HashSet<String>();
		if (!id.isEmpty())
		{
			String s = getRESTAPIRootRepresentation(id);
			JSONObject o =	getJSonResponse(s);
			affixes = getAllAffixesFromJSonRoot(o);
		}
		else
		{
			System.out.println("Word does not exist in OED: " + word);
			String file = resultPath + File.separator + "wordsNotExistentInOED" + wordclass + "" + affixtype + "_" + corpus.getPeriod() + "-" + corpus.getCorpusName() + ".csv";
			try
			{
				restapi.appendResultsToCSVFile(file, word);
			}
			catch (IOException e) 
			  {
				  System.out.println(e.getMessage());
			  }
		}
		return affixes;
	}
	/*
	 * delete the main method once programm is finished
	 * exist only for test purposes
	 */
//	public static void main(String[] args) 
//	{  
//		
//		String word = "mountainousness";
//		String morpheme ="ous";
//		String wordclass = "_nn01";
//		String affixtype = "_su01";
//		int startdate = 1570;
//		int enddate = 1639;
//		OED ox = new OED(word, morpheme, wordclass, affixtype, startdate, enddate);
//		ox.processOEDRequest(); 
//		
//	}
}
