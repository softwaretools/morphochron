import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Init
{
	/**
	 *  Short description of the algorithm given a PENN tagged text corpus
	 *  loop through list: for each word do:
	 *  0. reduce to verbs, nouns, and adjectives in three different lists
	 *  1. Hashmap with number of each word
	 *  2. reduce to word types
	 *  3. instantiate AffixStripper
	 *  4. delete if no lexical affix is present
	 *  5. write all words that contain affix to list
	 *  6. Check with Token-Hashmap if word in 5 is hapax legonoma
	 */

	
	public static void main(String[] args) throws IOException
	{
		/*
		 * Usage: specify which of the following corpora (2nd argument = period is optional)
		 * Corpus: period
		 * ppcme2: m1, m2, m3, m4
		 * ppceme: e1, e2, e3
		 * ppcmbe: Emod1, Emod2, Emod3
		 * 
		 * Word Class: _nn01, _vb01, _jj01
		 * 
		 * Affix Type: _su01, _pr01
		 * 
		 * TODO: 
		 * 1. implement GUI to have properties selected -- Done
		 * 2. include credentials and directories as selection -- Done
		 * 3. check if selected corpus is indeed ppc
		 * 4. procedure to incorporate postprocessingfiles in the result (i.e. merge list from postprocessing in morphemeWordList
		 */
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
					GUI_mainMenu frame = new GUI_mainMenu();
					frame.setVisible(true);
					
					frame.getRunButton().addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent e) 
						{
							if (frame.validateForm())
							{
							String corpus = frame.getCorpus();
							Set<String> periods = frame.getPeriods();
							String wordclass = frame.getWordClass();
							String affixtype = frame.getAffixType();
							String app_key = frame.getAppKey();
							String app_id = frame.getAppID();
							String corpusPath = frame.getCorpusPath();
							String resultPath = frame.getResultPath();
							
							for (String period : periods) 
							{
									frame.setMessage(
										"Selection made\ncorpus: " + corpus + "\nperiod: " + period + 
										"\nword class: " + wordclass + "\naffixtype: " + affixtype + "\n");
								
									Corpus cp = new Corpus(corpus, period, corpusPath);		
									ArrayList<String> allWordsOfCorpus = new ArrayList<String>();
									allWordsOfCorpus = cp.getCorpus();
									frame.setMessage("Corpus read completely and normalized\n");
								
									//System.getProperties().list(System.out);
									//create normalized word lists (factory pattern)
									WordClassFactory wordClassFactory = new WordClassFactory();
									WordClass wc = wordClassFactory.normalizeWords(wordclass, allWordsOfCorpus);
									
									ArrayList<String> normalizedWords = new ArrayList<String>();
									normalizedWords = wc.getNormalizedWords();
//									for (String word : normalizedWords)
//									{
//										System.out.println(word);
//									}
//									normalizedWords.add("mountainousness");
//									normalizedWords.add("mountainous");
//									normalizedWords.add("consideration");
//									normalizedWords.add("precondition");
//									normalizedWords.add("restlessness");
//									normalizedWords.add("dignitary");
//									normalizedWords.add("proposition");
//									normalizedWords.add("daskommtnichtvor");
//									normalizedWords.add("annoyaunce");
//									normalizedWords.add("assygnement");
//									normalizedWords.add("daskommtnichtvor");
									
								
									frame.setMessage("All words of type " + wordclass + " selected\n");
									//detect affixes in word list as a pre-processing and countercheck these with OED REST API
									Affix aff = new Affix(normalizedWords, cp, wordclass, affixtype, app_key, app_id, resultPath);
									Map<String, ArrayList<String>> morphemeWordList = new HashMap<String,ArrayList<String>>();
									Map<String, ArrayList<String>> notInOEDWordList = new HashMap<String,ArrayList<String>>();
									morphemeWordList = aff.getMorphemeWordList();
									notInOEDWordList = aff.getNotInOEDWordList();
									
									frame.setMessage("Affixes parsed and validated in OED\n");
									
									frame.setMessage("Writing results to file\n");
									//calculate results and write them to file
									try 
									{
										Result rs = new Result(morphemeWordList, notInOEDWordList, cp, normalizedWords, wordclass, affixtype, resultPath);
									} catch (Exception ex) {
										ex.printStackTrace();
									}
									frame.setMessage("Done!\n");
								}
							}
						}
					});	
			}
		});	
	}
}
