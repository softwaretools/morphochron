import java.util.ArrayList;

public class Adjective implements WordClass{

	
	private ArrayList<String> allAdjectivesOfCorpus = new ArrayList<String>();
	private ArrayList<String> allWordsOfCorpus = new ArrayList<String>();
	
	public void deleteInflections()
 	{
		for (String word : allWordsOfCorpus)
		{
			//the inflections -er/est and -ier/iest are left because inflected word 1. is found in OED and 2. is monomorphemic
			if (word.matches("[a-zA-Z]+[_|/](ADVR|ADJ|ADJR|ADJS|ADV|ADVS)"))
			{
				word = word.replaceAll("[_|/](ADVR|ADJR|ADJS|ADVS)", "");
				word = word.replaceAll("[_|/](ADJ|ADV)", "");
				allAdjectivesOfCorpus.add(word.toLowerCase());
			}
		}
 	}
	
	public void setWords(ArrayList<String> al)
	{
		this.allWordsOfCorpus = al;
		deleteInflections();
 	}
	
	public ArrayList<String> getNormalizedWords()
	{
 		return allAdjectivesOfCorpus;
 	}
}
